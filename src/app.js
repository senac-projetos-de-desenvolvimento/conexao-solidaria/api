const express = require('express');
const routes = require('./routes');
const path = require('path');
require('./config/db');

class App {
  constructor() {
    this.app = express();
    this.middlewares();
    this.routes();
  }
  middlewares() {
    this.app.use(express.json());
    this.app.use(
      '/files',
      express.static(path.resolve(__dirname, '..', 'tmp', 'uploads', 'home'))
    );
    
  }
  routes() {
    this.app.use(routes);
  }
}

module.exports = new App().app;
