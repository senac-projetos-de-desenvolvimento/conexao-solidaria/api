const { Model, DataTypes, UUID } = require('sequelize');

class Course extends Model {
    static init(sequelize) {
        super.init({
            name: DataTypes.STRING,
            description: DataTypes.STRING,
            ch: DataTypes.INTEGER,
            start_date: DataTypes.DATE,
            final_date: DataTypes.DATE,
        }, {
            sequelize,
            tableName: "courses",
            name: UUID,
            timestamps: true,
        });
    };

    static associate(models) {
        this.belongsToMany(models.Address, { foreignKey: 'course_id', through: 'address_courses', as: 'addresses' });
        this.hasMany(models.Classe, { foreignKey: 'course_id', as: 'classes' });
    };
};

module.exports = Course;