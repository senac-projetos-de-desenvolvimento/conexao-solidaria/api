const { Model, DataTypes, UUID } = require('sequelize');

class Classe extends Model {
    static init(sequelize) {
        super.init({
            name: DataTypes.STRING,
            term: DataTypes.STRING,
            capacity: DataTypes.INTEGER,
            schedule: DataTypes.TEXT,
            local: DataTypes.TEXT,
        }, {
            sequelize,
            tableName: "classes",
            name: UUID,
            timestamps: true,
        })
    }
    static associate(models) {
        this.belongsTo(models.Course, { foreignKey: 'course_id', as: 'course' });
    };
};

module.exports = Classe;