const { Model, DataTypes, UUID } = require('sequelize');

class User extends Model {
  static init(sequelize) {
    super.init({
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      phone: DataTypes.STRING,
    }, {
      sequelize,
      tableName: "users",
      email: UUID,
      timestamps: true,
    })
  }
  static associate(models) {
    this.belongsTo(models.Role, { foreignKey: 'role_id', as: 'role' });

  }
}

module.exports = User;

