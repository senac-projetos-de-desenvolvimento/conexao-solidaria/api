const { Model, DataTypes, UUID } = require('sequelize');

class Address extends Model {
    static init(sequelize) {
        super.init({
            street: DataTypes.STRING,
            number: DataTypes.INTEGER,
            complement: DataTypes.STRING,
            cep: DataTypes.STRING,
            city: DataTypes.STRING,
            uf: DataTypes.STRING,
        }, {
            sequelize,
            tableName: "addresses",
            timestamps: true,
        });
    };
    static associate(models) {
        this.belongsToMany(models.Course, { foreignKey: 'address_id', through: 'address_courses', as: 'courses' });
    };
};

module.exports = Address;