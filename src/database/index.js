const Sequelize = require('sequelize');
const dbConfig = require('../config/db');

const Role = require('../models/Role');
const User = require('../models/User');
const Course = require('../models/Course');
const CommunityClass = require('../models/Classe');
const Address = require('../models/Address');
//const Instructor = require('../models/Instructor');
//const Student = require('../models/Student');
//const Registration = require('../models/Registration');

const connection = new Sequelize(dbConfig);

Role.init(connection);
User.init(connection);
Course.init(connection);
CommunityClass.init(connection);
Address.init(connection);
//Instructor.init(connection);
//Student.init(connection);
//Registration.init(connection);

Role.associate(connection.models);
User.associate(connection.models);
Course.associate(connection.models);
CommunityClass.associate(connection.models);
Address.associate(connection.models);
//Instructor.associate(connection.models);
//Student.associate(connection.models);
//Registration.associate(connection.models);

module.exports = connection;