const express = require('express');
const routes = require('./routes');
const cors = require('cors');
require('./database');
require('dotenv').config();

const app = express();

app.use(express.json());

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "X-PINGOTHER, Content-Type, Authorization");
    app.use(cors());
    next();
  });

app.use(routes);

app.listen(process.env.PORT);

