const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
require('dotenv').config();
const User = require('../models/User');

module.exports = {

    async auth(req, res) {

        const user = await User.findOne({
            attributes: ['id', 'name', 'email', 'password', 'role_id'],
            where: {
                email: req.body.email
            }
        });
        if (user === null) {
            return res.status(400).json({
                erro: true,
                mensagem: "Erro: Usuário ou a senha incorreta!"
            });
        };

        if (!(await bcrypt.compare(req.body.password, user.password))) {
            return res.status(400).json({
                erro: true,
                mensagem: "Erro: Usuário ou a senha incorreta!"
            });
        };

        var token = jwt.sign({ id: user.id, level: 1 }, process.env.ADM, {
            expiresIn: 600 // 10min
            //expiresIn: '7d', // 7 dia
        });

        return res.json({
            erro: false,
            mensagem: "Login realizado com sucesso!",
            data: user,
            token,
        });
    },

    

    async valToken(req, res) {
        await User.findByPk(req.userId, { attributes: ['id', 'name', 'email', 'role_id'] })
        .then((user) => {
            return res.json({
                erro: false,
                mensagem: "Token válido!",
                user
            });
        }).catch(() => {
            return res.status(400).json({
                erro: true,
                mensagem: "Erro: Necessário realizar o login para acessar a página!"
            });
        });

        
    }
}

