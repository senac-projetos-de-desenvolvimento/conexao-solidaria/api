const Classe = require('../models/Classe');
const Course = require('../models/Course');

module.exports = {

    async mostrar(req, res) {

        await Classe.findAll({ order: [['id', 'DESC']] }).then(function (classes) {
            return res.json({
                erro: false,
                classes
            });
        }).catch(function () {
            return res.json({
                erro: true,
                mensagem: "Erro: Nenhuma turma encontrada!"
            });
        });
    },

    async list(req, res) {

        const classes = await Classe.findAll({
            attributes: ['id', 'course_id', 'name', 'term', 'capacity', 'schedule', 'local'], include: {
                association: 'course', attributes: ['name']
            }
        });
        return res.json({
            erro: false,
            mensagem: "Turma(s) encontrada(s)!",
            classes
        });

    },

    async view(req, res) {

        await Classe.findByPk(req.params.id).
            then(classe => {
                return res.json({
                    erro: false,
                    classe
                });
            }).catch(function () {
                return res.json({
                    erro: true,
                    messagem: "Erro: Usuário não encontrado!"
                });
            });
    },

    async store(req, res) {

        var data = req.body;

        const course = await Course.findByPk(data.course_id);

        if (!course) {
            return res.json({
                erro: true,
                mensagem: "Erro: Curso não encontrado!"
            })
        }

        const classe = await Classe.create(data).then(classe => {
            return res.json({
                erro: false,
                mensagem: "Turma cadastrada com sucesso!",
                classe
            });
        }).catch(function () {
            return res.json({
                erro: true,
                mensagem: "Erro: Turma não cadastrada com sucesso!"
            });
        });
    },

    async edit(req, res) {
        var data = req.body;

        await Classe.update(data, { where: { id: data.id } }).
            then(function () {
                return res.json({
                    erro: false,
                    mensagem: "Turma editada com sucesso!"
                });
            }).catch(function () {
                return res.json({
                    erro: true,
                    mensagem: "Erro: Turma não editada com sucesso!"
                });
            });
    },

    async delete(req, res) {
        await Classe.destroy({ where: { id: req.params.id } }).
            then(function () {
                return res.json({
                    erro: false,
                    mensagem: "Turma apagada com sucesso!"
                });
            }).catch(function () {
                return res.json({
                    erro: true,
                    mensagem: "Erro: Usuário não apagado com sucesso!"
                });
            });
    },
}
