const Role = require('../models/Role');
const User = require('../models/User');

module.exports = {

  async list(req, res) {

    await Role.findAll({ order: [['id', 'ASC']] }).then(function (roles) {
      return res.json({
        erro: false,
        roles
      });
    }).catch(function () {
      return res.json({
        erro: true,
        mensagem: "Erro: Nenhum usuário encontrado!"
      });
    });
  },

  async view(req, res) {

    await Role.findByPk(req.params.id).
      then(user => {
        return res.json({
          erro: false,
          user
        });
      }).catch(function () {
        return res.json({
          erro: true,
          messagem: "Erro: Role não encontrada!"
        });
      });
  },

  async store(req, res) {

    var data = req.body;

    await Role.create(data).then(function () {
      return res.json({
        erro: false,
        mensagem: "Nível de acesso cadastrado com sucesso!"
      });
    }).catch(function () {
      return res.json({
        erro: true,
        mensagem: "Nìvel de acesso não cadastrado com sucesso!"
      });
    });
  },

  async findOrCreate(req, res) {

    const roles = ['Estudante', 'Instrutor', 'Administrador', 'Tecnico', 'Autonomo', 'Voluntario'];

    const n = roles.length;

    let i = 0;
    do {
      a = roles[i];
      console.log(a);
      i++;
      await Role.findOrCreate({
        where: { rl: a }
      })
    } while (i < n);

    return res.json({
      mensagem: "Cadastrado com sucesso!",
    });

  },

  /*
   
    async store(req, res) {
      const { rl } = req.body;
      const role = await Role.create({ rl });
      return res.json(role);
    },
    */

  async edit(req, res) {

    const roleVerificada = req.body.rl;

    if (!roleVerificada) {
      return res.status(401).json({
        erro: true,
        mensagem: "Erro: Campo vazio!"

      })
    }

    var data = req.body;

    await Role.update(data, { where: { id: data.id } }).
      then(function () {
        return res.json({
          erro: false,
          mensagem: "Nível de acesso editado com sucesso!"
        });
      }).catch(function () {
        return res.json({
          erro: true,
          mensagem: "Erro: Cargo já existente!"
        });
      });
  },


  async delete(req, res) {

    const userVerificado = await User.findOne({ where: { role_id: req.params.id } });

    if (userVerificado) {
      return res.status(400).json({
        erro: true,
        mensagem: "Nível de acesso possui usuários cadastrados!"
      })
    }

    await Role.destroy({ where: { id: req.params.id } }).
      then(function () {
        return res.json({
          erro: false,
          mensagem: "Nível de acesso apagado com sucesso!"
        });
      }).catch(function () {
        return res.json({
          erro: true,
          mensagem: "Erro: Nível de acesso não apagado com sucesso!"
        });
      });

  },

}

