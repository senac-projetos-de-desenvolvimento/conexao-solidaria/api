Instalação do projeto

Criar um arquivo .env com os seguintes campos:

SECRET = '// chave 1 //'
ADM = '// chave 2 //'

DB_DIALECT = 'mysql'
DB = '// nome do banco de dados //'
DB_HOST= '// nome do servidor //'
DB_USER = '// usuário //'
DB_PASS = '// senha //'
PORT = // número da porta //


Como rodar o projeto baixado
Instalar todas as dependências indicadas pelo package.json
### npm install

SEQUÊNCIA PARA CRIAR O PROJETO
Criar o arquivo package
### npm init -y

Gerencia as requisições, rotas e URLs, entre outras funcionalidades
### npm install express

Rodar o projeto
### node app.js

Acessar o projeto no navegador
### http://localhost:8080/

Instalar o módulo para reiniciar o servidor sempre que houver alteração no código fonte, g significa globalmente
### npm install -g nodemon

Instalar o nodemon como uma dependência de desenvolvedor 
### npm install --save -D nodemon

Rodar o projeto usando o nodemon
### nodemon app.js

Instalar a dependência para JWT
### npm install --save jsonwebtoken

Gerenciar variáveis de ambiente
### npm install --save dotenv

Permitir acesso a API
### npm install --save cors

Verificar a versão do MySQL instalado na máquina
### mysql -u root -p

Instalar o driver do banco de dados
### npm install --save mysql2

Sequelize é uma biblioteca Javascript que facilita o gerenciamento de um banco de dados SQL
### npm install --save sequelize

Instalar o módulo para criptografar a senha
### npm install --save bcryptjs

Instalar a biblioteca sequelize-cli em modo de desenvolvedor
### npm install --save -D sequelize-cli


Banco de Dados

Criar uma migration
### npx sequelize migration:generate --name create-course.js

Criar o banco de dados
### npx sequelize db:create

Executar a migration 
### npx sequelize db:migrate

Executar a seed (criação de um registro)
### npx sequelize db:seed:all
